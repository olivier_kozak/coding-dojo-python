__author__ = 'okozak'

import collections

Student = collections.namedtuple('Student', 'name score')


def get_students_having_score_above(students, score):
    """Get the students having their score above the given one.

    >>> get_students_having_score_above([
    ...     Student('Olivier',   13),
    ...     Student('Dominique', 18),
    ...     Student('Thomas',    11),
    ... ], 12)
    [Student(name='Olivier', score=13), Student(name='Dominique', score=18)]

    """
    # def get_student_above(student):
    #     return student.score > score
    #
    # return filter(get_student_above, students)

    return filter(lambda student : student.score > score, students)
