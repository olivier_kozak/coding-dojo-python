__author__ = 'okozak'

import collections

Student = collections.namedtuple('Student', 'name score')


def calculate_average_score(students):
    """Calculates the average score.

    >>> calculate_average_score([
    ...     Student('Olivier',   13),
    ...     Student('Dominique', 18),
    ...     Student('Thomas',    11),
    ... ])
    14

    """

    return sum([student.score for student in students]) / len(students)
