__author__ = 'okozak'

import collections

#Student = collections.namedtuple('Student', 'name score')

class Student(object):
    def __init__(self, name, score):
        self.name, self.score = name, score


def get_highest_score(students):
    """Gets the highest score.

    >>> get_highest_score([
    ...     Student('Olivier',   13),
    ...     Student('Dominique', 18),
    ...     Student('Thomas',    11),
    ... ])
    18

    """
    # highest_score = 0
    # for student in students:
    #     if student.score > highest_score:
    #         highest_score = student.score
    # return highest_score

    # return sorted([student.score for student in students], reverse=True)[0]

    return max([student.score for student in students])
