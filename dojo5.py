__author__ = 'okozak'

import collections
from itertools import imap

Student = collections.namedtuple('Student', 'name score')


def are_all_scores_above(students, score):
    """Returns true if all the scores are above the given one.

    >>> are_all_scores_above([
    ...     Student('Olivier',   13),
    ...     Student('Dominique', 18),
    ...     Student('Thomas',    11),
    ... ], 10)
    True

    >>> are_all_scores_above([
    ...     Student('Olivier',   13),
    ...     Student('Dominique', 18),
    ...     Student('Thomas',    11),
    ... ], 12)
    False

    """

    #return all(student.score > score for student in students)
    # for student in students:
    #     if student.score < score:
    #         return False
    # return True
    #return all(imap(lambda s: s.score > score, students))

    def gen():
        for student in students:
            yield student.score > score

    return all(gen())

