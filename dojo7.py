__author__ = 'okozak'

import collections
import operator


class Student(collections.namedtuple('Student', 'name score')):
    def __str__(self):
        return '{name} => {score}'.format(name=self.name, score=self.score)


def to_string_from_highest_to_lowest_score(students):
    """Converts the students list to string from the highest to the lowest score.

    >>> to_string_from_highest_to_lowest_score([
    ...     Student('Olivier',   13),
    ...     Student('Dominique', 18),
    ...     Student('Thomas',    11),
    ... ])
    '1. Dominique => 18, 2. Olivier => 13, 3. Thomas => 11'

    """


    #return ", ".join([". ".join((str(pos+1), str(student))) for pos,student in enumerate(sorted(students, key=operator.attrgetter('score'), reverse=True))])

    def gen(students_list):
        sorted_list_of_stuff = sorted(students_list, key=operator.attrgetter('score'), reverse=True)
        for pos,student in enumerate(sorted_list_of_stuff, start=1):
            yield "{pos}. {std}".format(std=student, pos=pos)
    return ", ".join(gen(students_list=students))
