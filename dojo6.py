__author__ = 'okozak'

import collections


class Student(collections.namedtuple('Student', 'name score')):
    def __str__(self):
        return '{name} => {score}'.format(name=self.name, score=self.score)


def to_string(students):
    """Converts the students list to string.

    >>> to_string([
    ...     Student('Olivier',   13),
    ...     Student('Dominique', 18),
    ...     Student('Thomas',    11),
    ... ])
    'Olivier => 13, Dominique => 18, Thomas => 11'

    """

    return  ", ".join([student.__str__() for student in students])
         #return  ", ".join([str(student) for student in students])
    #return  ", ".join(map(Student.__str__, students))