from IN import be16toh

__author__ = 'okozak'

import collections
import operator

Student = collections.namedtuple('Student', 'name score')


def get_student_having_highest_score(students):
    """Get the student having the highest score.

    >>> get_student_having_highest_score([
    ...     Student('Olivier',   13),
    ...     Student('Dominique', 18),
    ...     Student('Thomas',    11),
    ... ])
    Student(name='Dominique', score=18)

    """
    # best_student = None
    # for student in students:
    #     if not best_student or student.score > best_student.score:
    #         best_student = student
    #
    # return best_student

    # def compare(student_a, student_b):
    #     return cmp(student_a.score, student_b.score)
    # return sorted(students, compare, reverse=True)[0]

    # def get_score(student):
    #     return student.score
    # return sorted(students, key=get_score, reverse=True)[0]

    # return sorted(students, key=lambda student : student.score, reverse=True)[0]

    return sorted(students, key=operator.attrgetter('score'), reverse=True)[0]
